import { browser, element, by } from 'protractor';

// Choose Identification Type
export enum IdentificationType {
    Id,
    Name,
    Css,
    Xpath,
    LinkText,
    PartialLinkText,
    ClassName,
    ButtonText,
    Model
}

// Generic Elements
export const LocatorGeneric = {
    genericNextButton: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/button[@type="submit"]'
    }
}

// Generic Sleep Wait Time Values
export const SHORTWAIT = 2000;
export const MEDIUMWAIT = 3000;
export const LONGWAIT = 5000;
export const VERYLONGWAIT = 10000;

// Common Functions
export class CommonTasks {

    // Generic Next Button
    genericNextButton = this.elementLocator(LocatorGeneric.genericNextButton);
    
    // Access URL
    static openBrowser(url: string, timeout: number) {
        browser.get(url, timeout);
        return browser.driver.wait(function () {
            // tslint:disable-next-line:no-shadowed-variable
            return browser.driver.getCurrentUrl().then(function (url) {
                return url.indexOf(url) > -1;
            });
        }, 5000);
    }

    // Add Wait
    static wait(number: any) {
        return browser.sleep(number);
    }

    // Select Value from Dropdown List
    // tslint:disable-next-line:no-shadowed-variable
    static selectValueFromDropdown(element: any, valueAtt: string) {
        return element.$('[value="' + valueAtt + '"]').click();
    }

    // Getting Required Date in Required Format
    static getDate(addDays: any, seperator: any) {
        const currentTime = new Date();
        const requiredTime = new Date(currentTime.getTime() + (1000 * 60 * 60 * 24 * addDays));
        const mm = (requiredTime.getMonth() + 1) < 10 ? '0' +
            (requiredTime.getMonth() + 1).toString() : (requiredTime.getMonth() + 1).toString()
        const dd = requiredTime.getDate() < 10 ? '0' + requiredTime.getDate().toString() : requiredTime.getDate().toString();
        return mm + seperator + dd + seperator + requiredTime.getFullYear().toString();
    }

    // Get Number Of Elements
    findCountOfElements(elements: any) {
        return elements.size();
    }

    // Identify Web Elements
    elementLocator(obj: any) {

        switch (obj.type) {
            case IdentificationType[IdentificationType.Xpath]:
                return element(by.xpath(obj.value));

            case IdentificationType[IdentificationType.Css]:
                return element(by.css(obj.value));

            case IdentificationType[IdentificationType.Id]:
                return element(by.id(obj.value));

            case IdentificationType[IdentificationType.Name]:
                return element(by.name(obj.value));

            case IdentificationType[IdentificationType.LinkText]:
                return element(by.linkText(obj.value));

            case IdentificationType[IdentificationType.PartialLinkText]:
                return element(by.partialLinkText(obj.value));

            case IdentificationType[IdentificationType.ClassName]:
                return element(by.className(obj.value));

            case IdentificationType[IdentificationType.ButtonText]:
                return element(by.buttonText(obj.value));

            case IdentificationType[IdentificationType.Model]:
                return element(by.model(obj.value));

            default:
                return element(by.xpath(obj.value));
        }
    }
}
