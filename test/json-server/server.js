var jsonServer = require('json-server');
var server = jsonServer.create();
var router = jsonServer.router('./test/json-server/db.json');
var middlewares = jsonServer.defaults();

server.use(middlewares);

server.use(jsonServer.rewriter({
    "/distributionCenter/distributionCentersForCompany" : "/distributionCenters",
    "/distributionCenter/saveDistributionCenters" : "/selectedDistributionCenter",
    "/distributionCenter/fetchSavedDistributionCenters?contractPriceProfileSeq=21" : "/fetchSavedDistributionCenters",
    "/customer/fetchCustomerId" : "/fetchCustomerId",
    "/contractPricing/savePricingInformation" : "/savePricingInformation",
    "/contractPricing/fetchCPPSequence?contractPriceProfileId=44" : "/fetchCPPSequence",
    "/markup/fetchMarkupDefaults?contractPriceProfileSeq=21&pricingEffectiveDate=*&pricingExpirationDate=*&contractName=*" : "/fetchMarkupDefaults",
    "/markup/addException" : "/addException",
    "/markup/renameMarkupException" : "/renameMarkupException",
    "/markup/deleteMarkupException?contractPriceProfileSeq=21&markupId=*&markupName=*" : "/deleteMarkupException/150",
    "/markup/fetchExceptionDefault?contractPriceProfileSeq=21&pricingEffectiveDate=*&pricingExpirationDate=*&exceptionName=*" : "/fetchExceptionDefault",
    "/markup/saveMarkupOnSell" : "/saveMarkupOnSell",
    "/splitcase/fetchSplitCase?contractPriceProfileId=21&pricingEffectiveDate=*&pricingExpirationDate=*" : "/fetchSplitCaseFee",
    "/splitcase/saveSplitCaseFee" : "/saveSplitCaseFee",
    "/markup/saveMarkup" : "/saveMarkup",
    "/review/fetchReviewData?contractPriceProfileSeq=21": "/reviewData",
}));


server.use(router);

server.listen(9002, function () {
    console.log('JSON Server is running');
});

