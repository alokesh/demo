import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { DistributionCenters, DistributionCenterList } from './distribution-center.model';
import { StepperService, STEPPER_URL, DistributionCenterService, STEPPER_NUMBERS } from './../../shared';

@Component({
  selector: 'app-distribution-centers',
  templateUrl: './distribution-centers.component.html',
  styleUrls: ['./distribution-centers.componet.scss']
})
export class DistributionCentersComponent
  implements OnInit, OnDestroy, DoCheck {
  public isActive: boolean[] = [false];
  public distributionCenters: DistributionCenters[] = [];
  private _subscription: Subscription[] = [];
  public selectedDistributionCenters: string[] = [];
  public validDistributionCenterList = true;
  public distributionCenterList = <DistributionCenterList>{};

  constructor(
    private _stepperService: StepperService,
    private _distributonService: DistributionCenterService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._stepperService.currentStep(STEPPER_NUMBERS.DISTRIBUTION_CENTERS);
    this._subscription.push(
      this._distributonService.getdistributionCenters().subscribe(response => {
        this.distributionCenters = response;
        this.initializeState();
        const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
        const cppSeqId = contractDetails.cppseqid;
        this._subscription.push(this._distributonService.getSelecteddistributionCenters(cppSeqId)
          .subscribe(response => {
            response.forEach((distributionCenter, index) => {
              this.isActive[distributionCenter.dcCode] = true;
            });
          }));
      })
    );
  }

  initializeState() {
    this.distributionCenters.forEach((distributionCenter, index) => {
      this.isActive[distributionCenter.dcNumber] = false;
    });
  }

  selectALL() {
    this.distributionCenters.forEach((distributionCenter) => {
      this.isActive[distributionCenter.dcNumber] = true;
    });
  }

  clear() {
    this.distributionCenters.forEach((distributionCenter) => {
      this.isActive[distributionCenter.dcNumber] = false;
    });
  }

  buildDistributionCenters() {
    const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
    const cppSeqId = Number(contractDetails.cppseqid);
    const effectiveDate = contractDetails.pstdate;
    const expirationDate = contractDetails.penddate;
    this.distributionCenterList.contractPriceProfileSeq = cppSeqId;
    this.distributionCenterList.distributionCenters = this.selectedDistributionCenters;
    this.distributionCenterList.effectiveDate = effectiveDate;
    this.distributionCenterList.expirationDate = expirationDate;
  }

  saveDistributionCenters(dcForm) {
    Object.keys(dcForm.value).forEach((element) => {
      if (this.isActive[element]) {
        this.selectedDistributionCenters.push(element);
      }
    });
    if (this.selectedDistributionCenters.length > 0) {
      this.validDistributionCenterList = true;
    } else {
      this.validDistributionCenterList = false;
    }
    if (this.validDistributionCenterList) {
      this.buildDistributionCenters();
      this._subscription.push(
        this._distributonService
          .saveDistributionCenters(this.distributionCenterList)
          .subscribe(() => {
            this._router.navigate([STEPPER_URL.MARKUP_URL], { relativeTo: this._route });
          })
      );
    }
  }

  ngDoCheck() {
    this.isActive.forEach(element => {
      if (element === true) {
        this.validDistributionCenterList = true;
      }
    });
  }

  ngOnDestroy() {
    this._subscription.forEach(sub => sub.unsubscribe());
  }
}
