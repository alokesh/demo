export interface DistributionCenters {
  dcNumber: string;
  name: string;
  shortName: string;
}


export interface DistributionCenterList {
  contractPriceProfileSeq: number;
  distributionCenters: string[];
  expirationDate: string;
  effectiveDate: string;
}

export interface SelectedDistributionCenters {
  dcCode: string;
  cppId: string;
  expirationDate: string;
  effectiveDate: string;
}
