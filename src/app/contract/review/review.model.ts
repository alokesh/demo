export interface MarkupGridModel {
  productType: string;
  itemPriceId: number;
  markup: string;
  unit: string;
  markupType: number;
  effectiveDate: string;
  expirationDate: string;
};

export interface SplitCaseFeesModel {
  productType: string;
  splitCaseFee: number;
  unit: string;
  effectiveDate: string;
  expirationDate: string;
  itemPriceId: string;
};

export interface MarkupGridDTO {
  markupName: string;
  markup: MarkupGridModel[];
};

export interface ContractPricingDTO {
  scheduleForCost: string;
  scheduleForCostContractLanguage?: string;
  formalPriceAudit: string;
  formalPriceAuditContractLanguage?: string;
  priceVerification: string;
  priceVerificationContractLanguage?: string;
  gfsTransferFee: string;
  gfsLabelAssesmentFee: string;
  costOfProductsContractLanguage?: string;
};

export interface MarkupReviewDTO {
  markupBasedOnSell: string;
  markupBasedOnSellContractLanguage1?: string;
  markupBasedOnSellContractLanguage2?: string;
  markupGridDTOs: MarkupGridDTO[];
}

export interface ReviewData {
  contractPricingReviewDTO: ContractPricingDTO;
  distributionCenter: string[];
  markupReviewDTO: MarkupReviewDTO;
  splitCaseFee: SplitCaseFeesModel[];
}

