import { CONTRACT_TYPES } from './../../shared/utils/app.constants';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';

import {  } from 'app/shared';
import { ReviewService, StepperService } from './../../shared';
import { ZERO, ONE, STEPPER_NUMBERS, STEPPER_URL, YES, NO } from './../../shared';
import { ReviewData } from 'app/contract/review/review.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit, OnDestroy {
  public contractType: string;
  public contractPriceProfileId: string;
  public alphabetSet = 'B';
  public next_val: string;
  private _subscription: Subscription[] = [];
  user: Observable<{}>;
  reviewData: ReviewData;
  dcList = [];
  markupGridData = [];
  labelYes = YES;
  labelNo = NO;
  contractTypeIFS = CONTRACT_TYPES.IFS;

  constructor(
    private _stepperService: StepperService,
    private _reviewService: ReviewService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}

  ngOnInit() {
    this._stepperService.currentStep(STEPPER_NUMBERS.REVIEW);
    this.getContractType();
    this.loadResponse();
    this._subscription.push(
      this._reviewService
        .fetchReviewData(this.contractPriceProfileId)
        .subscribe(response => {
          this.reviewData = response;
          this.dcList = response.distributionCenter;
          this.markupGridData = response.markupReviewDTO.markupGridDTOs;
          if (
            response.contractPricingReviewDTO
              .formalPriceAuditContractLanguage !== null ||
            response.contractPricingReviewDTO
              .priceVerificationContractLanguage !== null
          ) {
            this.next_val = this.incrementChar(this.alphabetSet);
          } else {
            this.next_val = this.alphabetSet;
          }
        })
    );
  }

  getContractType() {
    const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
    this.contractType = contractDetails.ctype;
    this.contractPriceProfileId = contractDetails.cppseqid;
  }

  loadResponse() {
    this.user = this._reviewService.fetchReviewData(
      this.contractPriceProfileId
    );

  }

  incrementChar(c) {
    return String.fromCharCode(c.charCodeAt(ZERO) + ONE);
  }

  navigateToContractPricing() {
    this._router.navigate([STEPPER_URL.PRICING_INFORMATION_URL], {
      relativeTo: this._route
    });
  }

  navigateToDistributionCenter() {
    this._router.navigate([STEPPER_URL.DISTRIBUTION_CENTERS_URL], {
      relativeTo: this._route
    });
  }

  navigateToMarkup() {
    this._router.navigate([STEPPER_URL.MARKUP_URL], {
      relativeTo: this._route
    });
  }

  navigateToSplitCase() {
    this._router.navigate([STEPPER_URL.SPLIT_CASE_URL], {
      relativeTo: this._route
    });
  }

  ngOnDestroy() {
    this._subscription.forEach(sub => sub.unsubscribe());
  }
}
