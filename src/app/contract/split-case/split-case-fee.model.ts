export interface SplitCaseFees {
  productType: string;
  splitCaseFee: number;
  unit: string;
  effectiveDate: string;
  expirationDate: string;
  itemPriceId: string;
 }

 export interface SplitCaseList {
  contractPriceProfileSeq: number;
  splitCaseFeeValues: SplitCaseFees[];
 }
