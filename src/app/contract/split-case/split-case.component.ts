import { Component, OnInit, Renderer2 } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';

import { StepperService, STEPPER_NUMBERS, STEPPER_URL, IMAGE_PATH, UNIT_TYPES, MAX_SPLITCASE_FEE,
  MIN_SPLITCASE_FEE, SPLITCASE_PATTERN, ZERO_VALUE } from './../../shared';
import { SplitcaseFeeService } from './../../shared';
import { SplitCaseFees, SplitCaseList } from './split-case-fee.model';

@Component({
  selector: 'app-split-case',
  templateUrl: './split-case.component.html',
  styleUrls: ['./split-case.component.scss']
})
export class SplitCaseComponent implements OnInit {
  public selected = [];
  public rows= [];
  public imageDir = IMAGE_PATH;
  private _subscription: Subscription[] = [];
  public splitCaseFee: SplitCaseFees[] = [];
  public SplitCaseFees = <SplitCaseFees>{};
  public splitCaseList = <SplitCaseList>{};

  constructor(
    private _stepperService: StepperService,
    private _renderer: Renderer2,
    private _router: Router,
    private _route: ActivatedRoute,
    private _splitCaseService: SplitcaseFeeService
  ) {}

  ngOnInit() {
    this._stepperService.currentStep(STEPPER_NUMBERS.SPLIT_CASE);
    this.getProductTypes();
  }

  getProductTypes() {
    const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
    const cppId = contractDetails.cppseqid;
    const effectiveDate = contractDetails.cstdate;
    const expirationDate = contractDetails.cenddate;

    this._subscription.push(
      this._splitCaseService
        .fetchProductTypes(cppId, effectiveDate, expirationDate)
        .subscribe(response => {
          this.rows = response;
        })
    );
  }

  onSplitcaseKeyPress($event, row) {
    const inputChar = String.fromCharCode($event.charCode);
    const updatedValue = ($event.target.value + inputChar).split('.');
    if (($event.keyCode !== 8 && !SPLITCASE_PATTERN.test(inputChar)) ||
      (updatedValue[1] && updatedValue[1].length > 2 ) || (updatedValue[0] && updatedValue[0].length > 2) || updatedValue.length > 2
    ) {
      $event.preventDefault();
    }
  }

  onblurSplitcase(event, row) {
    if (event.target.value === '.' || event.target.value === '') {
        row.splitCaseFee = ZERO_VALUE;
        this._renderer.setProperty(event.target, 'value', ZERO_VALUE);
    } else {
      const splitCaseVal = event.target.value ? DecimalPipe.prototype.transform(event.target.value, '1.2-2') : '';
      row.splitCaseFee = splitCaseVal.replace(/,/g, '');
      this._renderer.setProperty(event.target, 'value', splitCaseVal.replace(/,/g, ''));
    }
}

  validateData(row) {
    if (
      (row.splitCaseFee > MIN_SPLITCASE_FEE && row.unit === UNIT_TYPES.DOLLAR) ||
      (row.splitCaseFee > MAX_SPLITCASE_FEE && row.unit === UNIT_TYPES.PERCENT)
    ) {
      return true;
    } else {
      return false;
    }
  }

  onUnitClick(event, row) {
    row.unit = event.target.value;
  }

  copyRow(event, row) {
    const selectedSplitCaseFee = row.splitCaseFee;
    const selectedUnit = row.unit;
    const unit = row.unit;
    this.rows.forEach(splitCaseRowElement => {
      splitCaseRowElement.splitCaseFee = selectedSplitCaseFee;
      splitCaseRowElement.unit = selectedUnit;
    });
  }

  buildSplitCaseFees() {
    const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
    const cppSeqId = Number(contractDetails.cppseqid);
    this.splitCaseList.contractPriceProfileSeq = cppSeqId;
    this.splitCaseList.splitCaseFeeValues = this.rows;
  }

  saveSplitCaseFee() {
    this.buildSplitCaseFees();
    this._subscription.push(
      this._splitCaseService
        .saveSplitCaseFee(this.splitCaseList)
        .subscribe(() => {
          this._router.navigate([STEPPER_URL.REVIEW_URL], {
            relativeTo: this._route
          });
        })
    );
  }
}
