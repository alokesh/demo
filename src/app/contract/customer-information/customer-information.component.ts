import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import * as moment from 'moment';
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { CustomerInformation } from './customer-information.model';
import { CustomerInformationService, StepperService } from './../../shared';
import { DateUtils } from './../../shared/utils/date-utils';
import { PRICE_VERIFICATION_LABEL, PRICE_AUDIT_LABEL, STEPPER_URL, IMAGE_PATH, DATE_FORMAT_MMDDYYYY, CONTRACT_TYPES,
  DATE_FORMAT_PIPE, CALENDAR_TYPES, STEPPER_NUMBERS, TRANSFER_FEE_LABEL, ASSESSMENT_FEE_LABEL } from './../../shared';


@Component({
  selector: 'app-customer-information',
  templateUrl: './customer-information.component.html',
  styleUrls: ['./customer-information.component.scss']
})
export class CustomerInformationComponent implements OnInit, OnDestroy {

  public priceVerificationLabel = PRICE_VERIFICATION_LABEL;
  public priceAuditLabel = PRICE_AUDIT_LABEL;
  public transferFeeLabel = TRANSFER_FEE_LABEL;
  public assessmentFeeLabel = ASSESSMENT_FEE_LABEL;
  public defaultToggleOn = true;
  public defaultToggleOff = false;
  public dateFormat = DATE_FORMAT_PIPE;
  public contractTypeIFS = CONTRACT_TYPES.IFS;
  public imageDir = IMAGE_PATH;
  private subscription: Subscription[] = [];
  public custInfoForm: FormGroup;
  public pricingEffectiveDate: NgbDateStruct;
  public contractStartDate: NgbDateStruct;
  public contractEndDate: NgbDateStruct;
  public pricingExpirationDate: string;
  public contractName: string;
  public selectedContractType: string;
  public ctStartDate: string;
  public ctEndDate: string;
  public cppId: string;
  private now = new Date();
  public currentDate = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};
  public defaultChecked = true;
  public assessmentFeeVal = true;
  public transferFeeVal = true;
  public priceAuditVal = false;
  public priceVerificationVal = false;
  public customerInformation = <CustomerInformation>{};

  constructor(private _stepperService: StepperService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    private _NgbDateParserFormatter:  NgbDateParserFormatter,
    private _customerInformationService: CustomerInformationService
  ) {}

  ngOnInit() {
    this.loadForm();
    this._stepperService.currentStep(STEPPER_NUMBERS.PRICING_INFORMATION);
    this.contractName = this._route.snapshot.queryParams['cname'];
    this.selectedContractType = this._route.snapshot.queryParams['ctype'];
    this.ctStartDate = this._route.snapshot.queryParams['stdate'];
    this.ctEndDate = this._route.snapshot.queryParams['enddate'];
    this.cppId = this._route.snapshot.queryParams['cppid'];

    if (!this.contractName) {
      this.fetchContractInfo();
    }

    if (this.selectedContractType === CONTRACT_TYPES.IFS) {
      this._stepperService.contractTypeChange(this.selectedContractType);
    }
    this.contractStartDate =  this._NgbDateParserFormatter.parse(this.ctStartDate);
    this.contractEndDate = this._NgbDateParserFormatter.parse(this.ctEndDate);
    this.pricingExpirationDate = this.ctEndDate;
    if (!this.pricingEffectiveDate) {
      this.pricingEffectiveDate =  this.contractStartDate;
    }
  }

  loadForm() {
    this.custInfoForm = this._formBuilder.group({
      effectiveDate: new FormControl(null, [Validators.required, this.dateValidator.bind(this)]),
      calendar: new FormControl(CALENDAR_TYPES.FISCAL_CALENDAR),
      toggleQuestion: new FormControl('false'),
      priceVerificationToggle: this._formBuilder.group({
        selectedValue: [false],
      }),
      priceAuditToggle: this._formBuilder.group({
        selectedValue: [false],
      }),
      transferFeeToggle: this._formBuilder.group({
        selectedValue: [false],
      }),
      assessmentFeeToggle: this._formBuilder.group({
        selectedValue: [false],
      })
    });
  }

  fetchContractInfo() {
      const contractDetails = JSON.parse(sessionStorage.getItem('contractInfo'));
      this.contractName = contractDetails.cname;
      this.selectedContractType = contractDetails.ctype;
      this.ctStartDate = contractDetails.cstdate;
      this.ctEndDate = contractDetails.cenddate;
      this.pricingEffectiveDate = this._NgbDateParserFormatter.parse(contractDetails.pstdate);
      this.cppId = contractDetails.cppid;
  }


  dateValidator(control: FormControl) {
    const input = control.value;
    const pricingDate = moment(DateUtils.formatDate(this.pricingEffectiveDate)).startOf('day').toDate();
    const contractStDate = moment(DateUtils.formatDate(this.contractStartDate)).endOf('day').toDate();
    const contractEndDate = moment(DateUtils.formatDate(this.contractEndDate)).startOf('day').toDate();
    const currentDate = moment(DateUtils.formatDate(this.currentDate)).startOf('day').toDate();

    if ( input && !this.isDateObject(input)) {
          return { 'invalidDateFormat' : true };
      } else if ( this.pricingEffectiveDate && this.contractStartDate &&
        this.isDateObject(input)  && pricingDate.getTime() > contractStDate.getTime()) {
          return { 'invalidPricingContratDate': true };
      } else if ( this.pricingEffectiveDate && this.currentDate &&
        this.isDateObject(input)  && pricingDate.getTime() < currentDate.getTime() ) {
          return { 'invalidPricingCurrentDate': true };
      } else {
          return null;
      }
  }


  isDateObject( date: NgbDateStruct ) {
    const dateString = (this._NgbDateParserFormatter.format(date));
    if (moment(dateString, DATE_FORMAT_MMDDYYYY, true).isValid()) {
        return true;
    } else {
        return false;
    }
  }

   ontoggleQuestionNo() {
    this.priceVerificationVal = false;
    this.priceAuditVal = false;
    this.custInfoForm.get('priceVerificationToggle.selectedValue').setValue(false);
    this.custInfoForm.get('priceAuditToggle.selectedValue').setValue(false);
    this.custInfoForm.get('transferFeeToggle.selectedValue').setValue(false);
    this.custInfoForm.get('assessmentFeeToggle.selectedValue').setValue(false);
   }

   onPriceVerificationChange(selectedValue) {
     this.priceVerificationVal = selectedValue;
      if (!this.priceVerificationVal &&  !this.custInfoForm.get('priceAuditToggle.selectedValue').value) {
        this.custInfoForm.get('transferFeeToggle.selectedValue').setValue(false);
        this.custInfoForm.get('assessmentFeeToggle.selectedValue').setValue(false);
      }
   }

   onPriceAuditChange(selectedValue) {
     this.priceAuditVal = selectedValue;
     if (!this.priceAuditVal &&  !this.custInfoForm.get('priceVerificationToggle.selectedValue').value) {
      this.custInfoForm.get('transferFeeToggle.selectedValue').setValue(false);
      this.custInfoForm.get('assessmentFeeToggle.selectedValue').setValue(false);
    }
   }

  validateAllFormFields() {
    Object.keys(this.custInfoForm.controls).forEach(control => {
      const formControl = this.custInfoForm.get(control);
      formControl.markAsTouched({ onlySelf: true });
    });
  }

  buildCustomerInformation() {
 	this.customerInformation.contractName = this.contractName;
    this.customerInformation.contractType = this.selectedContractType;
    this.customerInformation.contractPriceProfileId = Number(this.cppId);
    this.customerInformation.pricingEffectiveDate = this._NgbDateParserFormatter.format(this.pricingEffectiveDate)
    this.customerInformation.pricingExpirationDate = this.pricingExpirationDate;
    this.customerInformation.scheduleForCostChange = this.custInfoForm.get('calendar').value;
    this.customerInformation.priceVerificationFlag = this.custInfoForm.get('priceVerificationToggle.selectedValue').value;
    this.customerInformation.priceAuditFlag = this.custInfoForm.get('priceAuditToggle.selectedValue').value;
    this.customerInformation.transferFeeFlag = this.custInfoForm.get('transferFeeToggle.selectedValue').value;
    this.customerInformation.assessmentFeeFlag = this.custInfoForm.get('assessmentFeeToggle.selectedValue').value;
  }

  saveCustomerDetails(cppSeqId: number) {
    const contractInformation = JSON.parse(sessionStorage.getItem('contractInfo'));
    contractInformation.pstdate = this._NgbDateParserFormatter.format(this.pricingEffectiveDate);
    contractInformation.cppseqid = cppSeqId;
    sessionStorage.setItem('contractInfo', JSON.stringify(contractInformation));
  }

  onSubmit() {
    if (this.custInfoForm.valid) {
      this.buildCustomerInformation();
      this.subscription.push(this._customerInformationService.savePricingInformation(this.customerInformation)
        .subscribe(() => {
          this.subscription.push(this._customerInformationService.fetchCPPSequenceId(this.cppId)
            .subscribe(response => {
              const cppSeqId = response.contractPriceProfileSeq;
              this.saveCustomerDetails(cppSeqId);
              this.navigateToUrl();
            }));
        }));
    } else {
      this.validateAllFormFields();
    }
  }

   navigateToUrl() {
    if (this.selectedContractType !== CONTRACT_TYPES.IFS) {
      this._router.navigate([STEPPER_URL.DISTRIBUTION_CENTERS_URL], {relativeTo: this._route});
    } else {
      this._router.navigate([STEPPER_URL.MARKUP_URL], {relativeTo: this._route});
    }
  }

  ngOnDestroy() {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
