export interface CustomerInformation {
     contractName: string;
     contractType: string;
     contractPriceProfileId: number;
     pricingEffectiveDate: string;
     pricingExpirationDate: string;
     scheduleForCostChange: string;
     priceVerificationFlag: boolean;
     priceAuditFlag: boolean;
     transferFeeFlag: boolean;
     assessmentFeeFlag: boolean;
     contractPriceProfileSeq?: number
}
