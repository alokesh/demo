import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { CustomerInformationComponent } from './contract/customer-information/customer-information.component';
import { DistributionCentersComponent } from './contract/distribution-centers/distribution-centers.component';
import { LaunchScreenComponent } from './launch-screen/launch-screen.component';
import { MarkupComponent } from './contract/markup/markup.component';
import { ReviewComponent } from './contract/review/review.component';
import { SplitCaseComponent } from './contract/split-case/split-case.component';
import { StepperComponent } from './contract/stepper/stepper.component';


const ROUTES: Routes = [
  { path: '', component: LaunchScreenComponent }, // This component will be deleted in future
  {
    path: '', component: StepperComponent,
    children: [
      { path: 'pricinginformation', component: CustomerInformationComponent },
      { path: 'distributioncenters', component: DistributionCentersComponent },
      { path: 'markup', component: MarkupComponent},
      { path: 'splitcasefee', component: SplitCaseComponent},
      { path: 'review', component: ReviewComponent},
      { path: '', redirectTo: 'pricinginformation', pathMatch: 'full' }
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})

export class AppRoutingModule {}
