import { Injectable, Injector } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { TranslatorService } from './../translate/translator.service';

@Injectable()
export class ToasterService {
  translatorService: TranslatorService;
  constructor(private toastr: ToastrService, private inj: Injector) {}

  showSuccess(message: string, title: string) {
    this.toastr.success(message, title, {
      positionClass: 'toast-top-right',
      enableHtml: true,
      timeOut: 4000,
      closeButton: true
    });
  }

  showError(error: any) {
    this.translatorService = this.inj.get(TranslatorService);
    const message = this.translatorService.translate('COMMON.ERROR_MSG');
    this.toastr.error(message, '', {
      positionClass: 'toast-top-right',
      enableHtml: true,
      timeOut: 0,
      closeButton: true
    });
  }
}
