import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ToasterService } from './../toaster/toaster.service';

@Injectable()
export class CppHttpInterceptor implements HttpInterceptor {
    constructor(private toaster: ToasterService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
        return next.handle(req.clone({ })).do(
            (httpEvent: HttpEvent<any>) => {
             },
            (error: any) => {
                this.toaster.showError(error.message);
            }
        );
    }
}
