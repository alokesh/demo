import { environment } from './../../../environments/environment';

let imagePath = '../../../';
if (environment.production) {
              imagePath = './';
}

export const IMAGE_PATH = imagePath;

export const PRICE_VERIFICATION_LABEL = 'Price Verification Privileges';

export const PRICE_AUDIT_LABEL = 'Formal Price Audit Privileges';

export const TRANSFER_FEE_LABEL = 'Include Transfer Fees (includes SMR and Planned Transfer fees)';

export const MARKUP_ON_SELL = 'Markup based on Sell';

export const EXPIRE_LOWER = 'Expire Lower';

export const ASSESSMENT_FEE_LABEL = 'Include GFS Label Assessment Fee';

export const DATE_FORMAT_MMDDYYYY = 'MM/DD/YYYY';

export const DATE_FORMAT_YYYYMMDD = 'YYYY-MM-DD';

export const DATE_FORMAT_PIPE = 'MM/dd/yyyy';

export const BTN_MARKUP_SAVED = 'Markup Saved';

export const BTN_SAVE_MARKUP = 'Save Markup';

export const MARKUP_PATTERN = /[0.00-9\.]/;

export const BACKSPACE_KEY = 8;

export const ZERO = 0;

export const ONE = 1;

export const YES = 'Yes';

export const NO = 'No';

export const MIN_MARKUP_DOLLAR = 0.05;

export const MAX_MARKUP_DOLLAR = 5.00;

export const MIN_MARKUP_PERCENT = 5.00;

export const MAX_MARKUP_PERCENT = 45.00;

export const MIN_SPLITCASE_FEE = 5.00;

export const MAX_SPLITCASE_FEE = 49.00;

export const ZERO_VALUE = '0.00';

export const SPLITCASE_PATTERN = /[0.00-9\.]/;

export const MAX_MARKUP_EXCEPTIONS = 20;

export const STEPPER_NUMBERS = {
  PRICING_INFORMATION: 1,
  DISTRIBUTION_CENTERS: 2,
  MARKUP: 3,
  SPLIT_CASE: 4,
  REVIEW: 5
};

export const STEPPER_URL = {
  PRICING_INFORMATION_URL: '/pricinginformation',
  DISTRIBUTION_CENTERS_URL: '/distributioncenters',
  MARKUP_URL: '/markup',
  SPLIT_CASE_URL: '/splitcasefee',
  REVIEW_URL: '/review'
};

export const STEPPER_LABEL = {
  PRICING_INFORMATION: 'Contract Pricing Information',
  DISTRIBUTION_CENTERS: 'Distribution Centers',
  MARKUP: 'Markup',
  SPLIT_CASE: 'Split Case Fee',
  REVIEW: 'Review'
};

export const CONTRACT_TYPES = {
  NC: 'NC',
  RC: 'RC',
  IFS: 'IFS',
  DAN: 'Distribution Agreement National',
  DAR: 'Distribution Agreement Regional',
  IF: 'Independent Food Service (IFS)',
  SDAN: 'DAN',
  SDAR: 'DAR'
};

export const CALENDAR_TYPES = {
  FISCAL_CALENDAR: 'fiscal',
  GREGORIAN_CALENDAR: 'gregorian'
}

export const MARKUP_TYPES = {
  SELL_UNIT: 1,
  PER_CASE: 2
}

export const UNIT_TYPES = {
  DOLLAR: '$',
  PERCENT: '%'
}


