import * as moment from 'moment';
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DATE_FORMAT_YYYYMMDD } from './../../shared';

export class DateUtils {

  public static formatDate(date: any): string {
    if (date) {
      return moment({
        year: date.year,
        month: date.month - 1,
        date: date.day
      }).format(DATE_FORMAT_YYYYMMDD);
    }
  }

  constructor() {}
}
