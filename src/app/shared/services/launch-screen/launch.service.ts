import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CustomerInformation } from './../../../contract';
import { apiUrls } from './../app.url';

@Injectable()
export class LaunchService {

  private fetchCustomerIdURL = apiUrls.fetchCustomerIdURL;

  constructor(private http: HttpClient) {}

    public getCustomerId():  Observable<CustomerInformation> {
      return this.http
      .get(this.fetchCustomerIdURL)
      .map((res: CustomerInformation) => res);
    }

}
