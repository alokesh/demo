import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { apiUrls } from './../app.url';

@Injectable()
export class ReviewService {
  private reviewURL = apiUrls.fetchReviewDataURL;

  constructor(private http: HttpClient) { }

  public fetchReviewData(contractPriceProfileId: string): Observable<any>  {
    let params = new HttpParams();
    params = params.set('contractPriceProfileSeq', contractPriceProfileId);
    return this.http
      .get(this.reviewURL, {params})
      .map(res => res);
  }


}
