import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { MarkupGridModel, MarkupGridDetails, SaveMarkupGridModel, ExceptionModel, SaveMarkupOnSellModel,
  RenameMarkupModel } from './../../../contract';
import { apiUrls } from './../app.url';

@Injectable()
export class MarkupService {

  private fetchMarkupGridURL = apiUrls.fetchMarkupGridURL;
  private saveMarkupGridURL = apiUrls.saveMarkupGridURL;
  private addExceptionURL = apiUrls.addExceptionURL;
  private fetchExceptionDefaultURL = apiUrls.fetchExceptionDefaultURL;
  private saveMarkupOnSellURL = apiUrls.saveMarkupOnSellURL;
  private renameMarkupExceptionURL = apiUrls.renameMarkupExceptionURL;
  private deleteMarkupExceptionURL = apiUrls.deleteMarkupExceptionURL;

    constructor(private http: HttpClient) {}

    public getMarkupGridDetails(contractPriceProfileId: string, effectiveDate: string, expirationDate: string,
      contractName: string):  Observable<MarkupGridDetails[]> {
      let params = new HttpParams();
      params = params.set('contractPriceProfileSeq', contractPriceProfileId);
      params = params.set('pricingEffectiveDate', effectiveDate);
      params = params.set('pricingExpirationDate', expirationDate);
      params = params.set('contractName', contractName);
      return this.http
        .get(this.fetchMarkupGridURL, {params})
        .map((res: MarkupGridDetails[]) => res);
    }

    public saveMarkupGridDetails(saveMarkupDetails: SaveMarkupGridModel): Observable<SaveMarkupGridModel> {
      return this.http
      .post(this.saveMarkupGridURL, saveMarkupDetails)
      .map((res: SaveMarkupGridModel) => res);
    }

    public addExceptionDetails(exceptionDetails: ExceptionModel): Observable<ExceptionModel> {
      return this.http
      .post(this.addExceptionURL, exceptionDetails)
      .map((res: ExceptionModel) => res);
    }

    public getExceptionDefaults(contractPriceProfileId: string, effectiveDate: string, expirationDate: string,
      exceptionName: string):  Observable<MarkupGridDetails> {
      let params = new HttpParams();
      params = params.set('contractPriceProfileSeq', contractPriceProfileId);
      params = params.set('pricingEffectiveDate', effectiveDate);
      params = params.set('pricingExpirationDate', expirationDate);
      params = params.set('exceptionName', exceptionName);
      return this.http
        .get(this.fetchExceptionDefaultURL, {params})
        .map((res: MarkupGridDetails) => res);
    }

    public saveMarkupOnSell(saveMarkupOnSellDetails: SaveMarkupOnSellModel): Observable<SaveMarkupOnSellModel> {
      return this.http
      .post(this.saveMarkupOnSellURL, saveMarkupOnSellDetails)
      .map((res: SaveMarkupOnSellModel) => res);
    }

    public renameMarkupException(renameMarkupModel: RenameMarkupModel): Observable<RenameMarkupModel> {
      return this.http
      .post(this.renameMarkupExceptionURL, renameMarkupModel)
      .map((res: RenameMarkupModel) => res);
    }

    public deleteMarkupException(contractPriceProfileId: string, markupId: string, markupName: string): Observable<any> {
      let params = new HttpParams();
      params = params.set('contractPriceProfileSeq', contractPriceProfileId);
      params = params.set('markupId', markupId);
      params = params.set('markupName', markupName);
      return this.http
      .delete(this.deleteMarkupExceptionURL, {params})
      .map((res: any) => res);
    }

}
