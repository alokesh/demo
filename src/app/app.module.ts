// Ng imports
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

// third party
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


//  cpp imports
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContractInformationComponent } from './contract';
import { CustomerInformationComponent } from './contract';
import { CustomerInformationService } from './shared';
import { CppHttpInterceptor } from './shared';
import { DistributionCenterService } from './shared';
import { DistributionCentersComponent } from './contract';
import { FooterComponent } from './layout';
import { HeaderComponent } from './layout';
import { LaunchService } from './shared/services/launch-screen/launch.service';
import { LaunchScreenComponent } from './launch-screen';
import { MarkupComponent } from './contract';
import { MarkupGridComponent } from './contract';
import { MarkupService } from './shared';
import { NgbDateMomentParserFormatter } from './shared';
import { ReviewService } from './shared';
import { ReviewComponent } from './contract';
import { SplitcaseFeeService } from './shared';
import { StepperService } from './shared';
import { StepperComponent } from './contract';
import { SplitCaseComponent } from './contract';
import { ToggleComponent } from 'app/shared';
import { ToasterService } from './shared';
import { TranslatorService } from './shared';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

// AoT requires an exported function for factories
export function NgbDateMomentParserFormatterFactory() {
  return new NgbDateMomentParserFormatter('MM/DD/YYYY');
}

@NgModule({
  declarations: [
    AppComponent,
    CustomerInformationComponent,
    DistributionCentersComponent,
    StepperComponent,
    ToggleComponent,
    HeaderComponent,
    FooterComponent,
    ContractInformationComponent,
    LaunchScreenComponent,
    MarkupComponent,
    MarkupGridComponent,
    SplitCaseComponent,
    ReviewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [
    {
      provide: NgbDateParserFormatter,
      useFactory: NgbDateMomentParserFormatterFactory,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CppHttpInterceptor,
      multi: true
    },
    StepperService,
    CustomerInformationService,
    DistributionCenterService,
    LaunchService,
    TranslatorService,
    ToasterService,
    MarkupService,
    SplitcaseFeeService,
    ReviewService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
